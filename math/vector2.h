/* _VECTOR2_H_
 * contains float & double versions of
 * 2 dimensional vector operations
 */
////////////////////FLOAT////////////////////
INLINE float dotproduct2f(float vec[2],float vec2[2]);
INLINE float crossproduct2f(float vec[2],float vec2[2]);
INLINE float vectorlength2f(float vec[2]);
INLINE void normalizevector2f(float result[2],float vec[2]);
INLINE void copyvector2f(float result[2],float vec[2]);
INLINE void negvector2f(float result[2],float vec[2]);
INLINE void zerovector2f(float result[2]);
INLINE void addvectors2f(float result[2],float vec[2],float vec2[2]);
INLINE void subtractvectors2f(float result[2],float vec[2],float vec2[2]);
INLINE void scalevector2f(float result[2],float vec[2],float scale);
INLINE void scaleaddvectors2f(float result[2],float vec[2],float vec2[2],float scale);
INLINE void tangentvector2f(float result[2],float vec[2]);
INLINE float vectordistance2f(float vec[2],float vec2[2]);
void rotatevector2f(float result[2],float rotationangle);

float xaxis2f[2]={ 1.0f,0.0f };
float yaxis2f[2]={ 0.0f,1.0f };
////////////////////DOUBLE////////////////////
INLINE double dotproduct2d(double vec[2],double vec2[2]);
INLINE double crossproduct2d(double vec[2],double vec2[2]);
INLINE double vectorlength2d(double vec[2]);
INLINE void normalizevector2d(double result[2],double vec[2]);
INLINE void copyvector2d(double result[2],double vec[2]);
INLINE void negvector2d(double result[2],double vec[2]);
INLINE void zerovector2d(double result[2]);
INLINE void addvectors2d(double result[2],double vec[2],double vec2[2]);
INLINE void subtractvectors2d(double result[2],double vec[2],double vec2[2]);
INLINE void scalevector2d(double result[2],double vec[2],double scale);
INLINE void scaleaddvectors2d(double result[2],double vec[2],double vec2[2],double scale);
INLINE void tangentvector2d(double result[2],double vec[2]);
INLINE double vectordistance2d(double vec[2],double vec2[2]);
void rotatevector2d(double result[2],double rotationangle);

double xaxis2d[2]={ 1.0,0.0 };
double yaxis2d[2]={ 0.0,1.0 };
