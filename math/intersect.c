int lineintersectplane(float intersectpoint[3],float normal[3],float *scale,float startpoint[3],float endpoint[3],float planepoint[3])
  {
  float vec[3];
  float dot;

  subtractvectors3f(vec,planepoint,endpoint);
  dot=dotproduct3f(vec,normal);
  if (dot<0.0f)
    return(0);

  subtractvectors3f(vec,planepoint,startpoint);
  dot=dotproduct3f(vec,normal);
  if (dot>0.0f)
    return(0);

  *scale=dot;
  subtractvectors3f(vec,endpoint,startpoint);
  dot=dotproduct3f(vec,normal);
  if (dot!=0.0f)
    *scale/=dot;
  else
    return(0);

  scaleaddvectors3f(intersectpoint,startpoint,vec,*scale);
  return(1);
  }

int lineintersecttriangle(float intersectpoint[3],float normal[3],float *scale,float startpoint[3],float endpoint[3],float vertex[3],float vertex2[3],float vertex3[3])
  {
  float vec[3],vec2[3],vec3[3];

  if (lineintersectplane(intersectpoint,normal,scale,startpoint,endpoint,vertex))
    {
			
    subtractvectors3f(vec,vertex2,vertex);
    subtractvectors3f(vec2,intersectpoint,vertex);
    crossproduct3f(vec3,vec,vec2);
    if (dotproduct3f(vec3,normal)<0.0f)
      return(0);

    subtractvectors3f(vec,vertex3,vertex2);
    subtractvectors3f(vec2,intersectpoint,vertex2);
    crossproduct3f(vec3,vec,vec2);
    if (dotproduct3f(vec3,normal)<0.0f)
      return(0);

    subtractvectors3f(vec,vertex,vertex3);
    subtractvectors3f(vec2,intersectpoint,vertex3);
    crossproduct3f(vec3,vec,vec2);
    if (dotproduct3f(vec3,normal)<0.0f)
      return(0);

    return(1);
    }

  return(0);
  }

int lineintersectline(float intersectpoint[3],float normal[3],float *scale,float startpoint[3],float endpoint[3],float vertex1[3],float vertex2[3])
  {
  float vec[3],vec2[3];
  float dot1,dot2;

//invert?
  normal[2]=vertex2[2]-vertex1[2];
  normal[0]=vertex1[1]-vertex2[1];
  normal[1]=vertex2[0]-vertex1[0];

  subtractvectors3f(vec,startpoint,vertex1);
  subtractvectors3f(vec2,endpoint,vertex1);

  dot1=dotproduct3f(vec,normal);
  dot2=dotproduct3f(vec2,normal);

  if (dot1<0.0f)
    return(0);

  if (dot2>0.0f)
    return(0);

  normalizevector3f(normal,normal);

  subtractvectors3f(vec,vertex1,startpoint);
  *scale=dotproduct3f(vec,normal);
  subtractvectors3f(vec,endpoint,startpoint);
  *scale/=dotproduct3f(vec,normal);

  scaleaddvectors3f(intersectpoint,startpoint,vec,*scale);

  subtractvectors3f(vec,intersectpoint,vertex1);
  subtractvectors3f(vec2,vertex2,vertex1);

  if (dotproduct3f(vec,vec2)<0.0f)
    return(0);

  subtractvectors3f(vec,intersectpoint,vertex2);
  subtractvectors3f(vec2,vertex1,vertex2);

  if (dotproduct3f(vec,vec2)<0.0f)
    return(0);

  return(1);
  }