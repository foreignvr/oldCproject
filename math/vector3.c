INLINE float dotproduct3f(float vec[3],float vec2[3])
  {
  float result;

  result=vec[0]*vec2[0]+vec[1]*vec2[1]+vec[2]*vec2[2];

  return(result);
  }

INLINE void crossproduct3f(float result[3],float vec[3],float vec2[3])
  {
  result[0]=vec2[1]*vec[2]-vec2[2]*vec[1];
  result[1]=vec2[2]*vec[0]-vec2[0]*vec[2];
  result[2]=vec2[0]*vec[1]-vec2[1]*vec[0];
  }

INLINE float vectorlength3f(float vec[3])
  {
  float result;
  result=dotproduct3f(vec,vec);
  result=(float)sqrt(result);
  return(result);
  }

INLINE void normalizevector3f(float result[3],float vec[3])
  {
  float veclength = vectorlength3f(vec);
  if (veclength!=0.0f)
    {
		veclength = 1.0f/veclength;
    result[0]=vec[0]*veclength;
    result[1]=vec[1]*veclength;
    result[2]=vec[2]*veclength;
    }
  else
    {
    result[0]=0.0f;
    result[1]=0.0f;
    result[2]=0.0f;
    }
  }

INLINE void copyvector3f(float result[3],float vec[3])
  {
  result[0]=vec[0];
  result[1]=vec[1];
  result[2]=vec[2];
  }

INLINE void negvector3f(float result[3],float vec[3])
  {
  result[0]=-vec[0];
  result[1]=-vec[1];
  result[2]=-vec[2];
  }

INLINE void zerovector3f(float result[3])
  {
  result[0]=0.0f;
  result[1]=0.0f;
  result[2]=0.0f;
  }

INLINE void addvectors3f(float result[3],float vec[3],float vec2[3])
  {
  result[0]=vec[0]+vec2[0];
  result[1]=vec[1]+vec2[1];
  result[2]=vec[2]+vec2[2];
  }

INLINE void subtractvectors3f(float result[3],float vec[3],float vec2[3])
  {
  result[0]=vec[0]-vec2[0];
  result[1]=vec[1]-vec2[1];
  result[2]=vec[2]-vec2[2];
  }

INLINE void scalevector3f(float result[3],float vec[3],float scale)
  {
  result[0]=vec[0]*scale;
  result[1]=vec[1]*scale;
  result[2]=vec[2]*scale;
  }

INLINE void scaleaddvectors3f(float result[3],float vec[3],float vec2[3],float scale)
  {
  result[0]=vec[0]+vec2[0]*scale;
  result[1]=vec[1]+vec2[1]*scale;
  result[2]=vec[2]+vec2[2]*scale;
  }

INLINE void setvector3f(float result[3],float x,float y,float z)
  {
  result[0]=x;
  result[1]=y;
  result[2]=z;
  }
INLINE float vectordistance3f(float vec[3],float vec2[3])
  {
  float temp[3];
  float result;

  subtractvectors3f(temp,vec2,vec);
  result=vectorlength3f(temp);

  return(result);
  }

INLINE float vectordistancesquared3f(float vec[3],float vec2[3])
  {
  float temp[3];
  float result;

  subtractvectors3f(temp,vec2,vec);
  result=vectorlength3f(temp);

  return(result*result);
  }
void projectvector3f(float result[3],float vec[3],float orientation[3][3])
  {
  float temp[3];

  copyvector3f(temp,vec);
  result[0]=temp[0]*orientation[0][0]+temp[1]*orientation[1][0]+temp[2]*orientation[2][0];
  result[1]=temp[0]*orientation[0][1]+temp[1]*orientation[1][1]+temp[2]*orientation[2][1];
  result[2]=temp[0]*orientation[0][2]+temp[1]*orientation[1][2]+temp[2]*orientation[2][2];
  }
void unprojectvector3f(float result[3],float vec[3],float orientation[3][3])
  {
  float temp[3];
  copyvector3f(temp,vec);
  result[0]=temp[0]/orientation[0][0]+temp[1]/orientation[1][0]+temp[2]/orientation[2][0];
  result[1]=0.0f;//temp[0]/orientation[0][1]+temp[1]/orientation[1][1]+temp[2]/orientation[2][1];
  result[2]=temp[0]/orientation[0][2]+temp[1]/orientation[1][2]+temp[2]/orientation[2][2];
  }

void generatetrinormal3f(float normal[3],float vertex[3],float vertex2[3],float vertex3[3])
  {
  float vec[3],vec2[3];

  subtractvectors3f(vec,vertex2,vertex);
  subtractvectors3f(vec2,vertex3,vertex);
  crossproduct3f(normal,vec,vec2);

  normalizevector3f(normal,normal);
  }

void resetorientation3f(float orientation[3][3])
  {
  orientation[0][0]=1.0f;
  orientation[0][1]=0.0f;
  orientation[0][2]=0.0f;
  orientation[1][0]=0.0f;
  orientation[1][1]=1.0f;
  orientation[1][2]=0.0f;
  orientation[2][0]=0.0f;
  orientation[2][1]=0.0f;
  orientation[2][2]=1.0f;
  }

void rotatevector3f(float result[3],float rotationvector[3],float rotationangle)
  {
  float cosnormal[3],sinnormal[3];
  float axisnormal[3];
  float axislength;
  float cosine,sine;

  copyvector3f(axisnormal,rotationvector);

  cosine=cosf(rotationangle);
  sine=sinf(rotationangle);

  crossproduct3f(sinnormal,axisnormal,result);
  crossproduct3f(cosnormal,sinnormal,axisnormal);

  axislength=dotproduct3f(axisnormal,result);

  scalevector3f(result,axisnormal,axislength);
  scaleaddvectors3f(result,result,cosnormal,cosine);
  scaleaddvectors3f(result,result,sinnormal,sine);
  normalizevector3f(result,result);
  }

void rotateorientation3f(float orientation[3][3],float rotationvector[3],float rotationangle)
  {
  int count;
  float cosnormal[3],sinnormal[3];
  float axisnormal[3];
  float axislength;
  float cosine,sine;

  copyvector3f(axisnormal,rotationvector);

  cosine=cosf(rotationangle);
  sine=sinf(rotationangle);

  for (count=0;count<3;count++)
    {
    crossproduct3f(sinnormal,axisnormal,orientation[count]);
    crossproduct3f(cosnormal,sinnormal,axisnormal);

    axislength=dotproduct3f(axisnormal,orientation[count]);

    scalevector3f(orientation[count],axisnormal,axislength);
    scaleaddvectors3f(orientation[count],orientation[count],cosnormal,cosine);
    scaleaddvectors3f(orientation[count],orientation[count],sinnormal,sine);
    normalizevector3f(orientation[count],orientation[count]);
    }
  }
