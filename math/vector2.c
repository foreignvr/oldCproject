/* _VECTOR2_C_
 * defines the 2 dimensional vector
 * operations from vector2.h
 */
////////////////////FLOAT////////////////////
INLINE float dotproduct2f(float vec[2],float vec2[2])
  {
  float result;

  result=vec[0]*vec2[0]+vec[1]*vec2[1];

  return(result);
  }

INLINE float crossproduct2f(float vec[2],float vec2[2])
  {
  float result;
  result=vec2[0]*vec[1]-vec2[1]*vec[0];
  return(result);
  }

INLINE float vectorlength2f(float vec[2])
  {
  float result;

  result=dotproduct2f(vec,vec);
  result=(float)sqrt(result);

  return(result);
  }
///UNOPTIMIZED
INLINE void normalizevector2f(float result[2],float vec[2])
  {
  float veclength;
  veclength=vectorlength2f(vec);
  if (veclength!=0.0f)
    {
    veclength=1.0f/veclength;
    result[0]=vec[0]*veclength;
    result[1]=vec[1]*veclength;
    }
  else
    {
    result[0]=0.0f;
    result[1]=0.0f;
    }
  //scalevector2f(result, vec, 1.0f / sqrtf(dotproduct2f(vec,vec)) );
  }

INLINE void copyvector2f(float result[2],float vec[2])
  {
  result[0]=vec[0];
  result[1]=vec[1];
  }

INLINE void negvector2f(float result[2],float vec[2])
  {
  result[0]=-vec[0];
  result[1]=-vec[1];
  }

INLINE void zerovector2f(float result[2])
  {
  result[0]=0.0f;
  result[1]=0.0f;
  }

INLINE void addvectors2f(float result[2],float vec[2],float vec2[2])
  {
  result[0]=vec[0]+vec2[0];
  result[1]=vec[1]+vec2[1];
  }

INLINE void subtractvectors2f(float result[2],float vec[2],float vec2[2])
  {
  result[0]=vec[0]-vec2[0];
  result[1]=vec[1]-vec2[1];
  }

INLINE void scalevector2f(float result[2],float vec[2],float scale)
  {
  result[0]=vec[0]*scale;
  result[1]=vec[1]*scale;
  }

INLINE void scaleaddvectors2f(float result[2],float vec[2],float vec2[2],float scale)
  {
  result[0]=vec[0]+vec2[0]*scale;
  result[1]=vec[1]+vec2[1]*scale;
  }

INLINE void tangentvector2f(float result[2],float vec[2])
  {
  result[0]=-vec[1];
  result[1]=vec[0];
  }

INLINE float vectordistance2f(float vec[2],float vec2[2])
  {
  float temp[2];
  float result;

  subtractvectors2f(temp,vec2,vec);
  result=vectorlength2f(temp);

  return(result);
  }

void rotatevector2f(float result[2],float rotationangle)
  {
  float sinnormal[2];
  float cosine,sine;

  tangentvector2f(sinnormal,result);

  cosine=cosf(rotationangle);
  sine=sinf(rotationangle);

  scalevector2f(result,result,cosine);
  scaleaddvectors2f(result,result,sinnormal,sine);
  }
////////////////////DOUBLE////////////////////
INLINE double dotproduct2d(double vec[2],double vec2[2])
  {
  double result;

  result=vec[0]*vec2[0]+vec[1]*vec2[1];

  return(result);
  }

INLINE double crossproduct2d(double vec[2],double vec2[2])
  {
  double result;

  result=vec2[0]*vec[1]-vec2[1]*vec[0];

  return(result);
  }

INLINE double vectorlength2d(double vec[2])
  {
  double result;

  result=dotproduct2d(vec,vec);
  result=sqrt(result);

  return(result);
  }

INLINE void normalizevector2d(double result[2],double vec[2])
  {
  double veclength;

  veclength=vectorlength2d(vec);
  if (veclength!=0.0)
    {
    veclength=1.0/veclength;
    result[0]=vec[0]*veclength;
    result[1]=vec[1]*veclength;
    }
  else
    {
    result[0]=0.0;
    result[1]=0.0;
    }
  }

INLINE void copyvector2d(double result[2],double vec[2])
  {
  result[0]=vec[0];
  result[1]=vec[1];
  }

INLINE void negvector2d(double result[2],double vec[2])
  {
  result[0]=-vec[0];
  result[1]=-vec[1];
  }

INLINE void zerovector2d(double result[2])
  {
  result[0]=0.0;
  result[1]=0.0;
  }

INLINE void addvectors2d(double result[2],double vec[2],double vec2[2])
  {
  result[0]=vec[0]+vec2[0];
  result[1]=vec[1]+vec2[1];
  }

INLINE void subtractvectors2d(double result[2],double vec[2],double vec2[2])
  {
  result[0]=vec[0]-vec2[0];
  result[1]=vec[1]-vec2[1];
  }

INLINE void scalevector2d(double result[2],double vec[2],double scale)
  {
  result[0]=vec[0]*scale;
  result[1]=vec[1]*scale;
  }

INLINE void scaleaddvectors2d(double result[2],double vec[2],double vec2[2],double scale)
  {
  result[0]=vec[0]+vec2[0]*scale;
  result[1]=vec[1]+vec2[1]*scale;
  }

INLINE void tangentvector2d(double result[2],double vec[2])
  {
  result[0]=-vec[1];
  result[1]=vec[0];
  }

INLINE double vectordistance2d(double vec[2],double vec2[2])
  {
  double temp[2];
  double result;

  subtractvectors2d(temp,vec2,vec);
  result=vectorlength2d(temp);

  return(result);
  }

void rotatevector2d(double result[2],double rotationangle)
  {
  double sinnormal[2];
  double cosine,sine;

  tangentvector2d(sinnormal,result);

  cosine=cos(rotationangle);
  sine=sin(rotationangle);

  scalevector2d(result,result,cosine);
  scaleaddvectors2d(result,result,sinnormal,sine);
  }
