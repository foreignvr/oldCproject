INLINE float dotproduct3f(float vec[3],float vec2[3]);
INLINE void crossproduct3f(float result[3],float vec[3],float vec2[3]);
INLINE float vectorlength3f(float vec[3]);
INLINE void normalizevector3f(float result[3],float vec[3]);
INLINE void copyvector3f(float result[3],float vec[3]);
INLINE void negvector3f(float result[3],float vec[3]);
INLINE void zerovector3f(float result[3]);
INLINE void addvectors3f(float result[3],float vec[3],float vec2[3]);
INLINE void subtractvectors3f(float result[3],float vec[3],float vec2[3]);
INLINE void scalevector3f(float result[3],float vec[3],float scale);
INLINE void scaleaddvectors3f(float result[3],float vec[3],float vec2[3],float scale);
INLINE void setvector3f(float result[3],float x,float y,float z);
INLINE float vectordistance3f(float vec[3],float vec2[3]);
INLINE float vectordistancesquared3f(float vec[3],float vec2[3]);

void unprojectvector3f(float result[3],float vec[3],float orientation[3][3]);
void projectvector3f(float result[3],float vec[3],float orientation[3][3]);
void generatetrinormal3f(float normal[3],float vertex[3],float vertex2[3],float vertex3[3]);
void resetorientation3f(float orientation[3][3]);
void rotatevector3f(float result[3],float rotationvector[3],float rotationangle);
void rotateorientation3f(float orientation[3][3],float rotationvector[3],float rotationangle);

float xaxis[3]={ 1.0f,0.0f,0.0f };
float yaxis[3]={ 0.0f,1.0f,0.0f };
float zaxis[3]={ 0.0f,0.0f,1.0f };
