int lineintersectline2d(float intersectpoint[2],float normal[2],float *scale,float startpoint[2],float endpoint[2],float vertex1[2],float vertex2[2])
  {
  float vec[2],vec2[2];
  float dot1,dot2;

  normal[0]=vertex1[1]-vertex2[1];
  normal[1]=vertex2[0]-vertex1[0];

  subtractvectors2f(vec,startpoint,vertex1);
  subtractvectors2f(vec2,endpoint,vertex1);

  dot1=dotproduct2f(vec,normal);
  dot2=dotproduct2f(vec2,normal);

  if (dot1<0.0f)
    return(0);

  if (dot2>0.0f)
    return(0);

  normalizevector2f(normal,normal);

  subtractvectors2f(vec,vertex1,startpoint);
  *scale=dotproduct2f(vec,normal);
  subtractvectors2f(vec,endpoint,startpoint);
  *scale/=dotproduct2f(vec,normal);

  scaleaddvectors2f(intersectpoint,startpoint,vec,*scale);

  subtractvectors2f(vec,intersectpoint,vertex1);
  subtractvectors2f(vec2,vertex2,vertex1);

  if (dotproduct2f(vec,vec2)<0.0f)
    return(0);

  subtractvectors2f(vec,intersectpoint,vertex2);
  subtractvectors2f(vec2,vertex1,vertex2);

  if (dotproduct2f(vec,vec2)<0.0f)
    return(0);

  return(1);
  }

int sphereintersectline2d(float intersectpoint[2],float normal[2],float *scale,float point[2],float vertex1[2],float vertex2[2],float size)
  {
  float vec[2],vec2[2];
  float dot;

  subtractvectors2f(vec,point,vertex1);
  subtractvectors2f(vec2,vertex2,vertex1);

  if (dotproduct2f(vec,vec2)<0.0f)
    return(0);

  subtractvectors2f(vec,point,vertex2);
  subtractvectors2f(vec2,vertex1,vertex2);

  if (dotproduct2f(vec,vec2)<0.0f)
    return(0);

  normal[0]=vertex1[1]-vertex2[1];
  normal[1]=vertex2[0]-vertex1[0];

  normalizevector2f(normal,normal);

  subtractvectors2f(vec,point,vertex1);

  dot=dotproduct2f(vec,normal);

  if (fabs(dot)>size)
    return(0);

  scaleaddvectors2f(intersectpoint,point,normal,-dot);
  *scale=dot/size;

  return(1);
  }

int lineintersectcircle2f(float intersectpoint[2],float normal[2],float *scale,float startpoint[2],float endpoint[2],float point[2],float size)
  {
  float vec[2],vec2[2];
  float dot;
  float length;

  subtractvectors2f(normal,endpoint,startpoint);
  length=vectorlength2f(normal);
  scalevector2f(normal,normal,1.0f/length);

  subtractvectors2f(vec,point,startpoint);
  dot=dotproduct2f(vec,normal);
  if (dot<0.0f || dot>length)
    return(0);

  scaleaddvectors2f(vec,startpoint,normal,dot);

  subtractvectors2f(vec2,vec,point);
  length=vectorlength2f(vec2);
  if (length>size)
    return(0);

  length=sqrtf(size*size-length*length);
  scaleaddvectors2f(intersectpoint,vec,normal,-length);

  *scale=dot-length;

  return(1);
  }

int pointintersectcircle(float normal[2],float *scale,float point[2],float vertex[2],float radius)
  {
  subtractvectors2f(normal,point,vertex);
  *scale=vectorlength2f(normal);
  if (*scale<radius)
    {
    scalevector2f(normal,normal,1.0f/(*scale));
    *scale/=radius;
    return(1);
    }

  return(0);
  }
