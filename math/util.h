//carmack inverse square-root (approximation)

INLINE float invsqrt(float arg);
INLINE void auto_invsqrt(float* arg);
INLINE float lensq3f(float arg[], float arg2[]);
INLINE float lensq2f(float arg[], float arg2[]);

void mul_matrix4x4_vector4f(float result[4],float vec[4],float mat[16]);
void mul_matrix3x3_vector3f(float result[3],float vec[3],float mat[9]);
void mul_matrix2x2_vector2f(float result[2],float vec[2],float mat[4]);

INLINE void transpose_submatrix3x3_matrix4x4(float result[16], float mat[16]);
INLINE void submatrix3x3_matrix4x4(float result[9], float mat[16]);
INLINE void transpose_matrix3x3(float result[9], float mat[9]);