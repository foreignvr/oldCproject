/*INLINE float invsqrt(float arg)
	{
	float result;
	int i;
	i = *(int*)&arg;
	i = 0x5f3759df - (i >> 1);
	result = *(float*)&i;
	result = (result * (1.5f - 0.5f * arg * result * result));
	return result;
	}*/
INLINE float lensq3f(float arg[], float arg2[])
	{
	float distx=(arg[0]-arg2[0]),disty=(arg2[1]-arg2[1]),distz=(arg2[2]-arg2[2]);
	return (distx*distx + disty*disty + distz*distz);
	}
INLINE float lensq2f(float arg[], float arg2[])
	{
	float distx=(arg[0]-arg2[0]),disty=(arg2[1]-arg2[1]);
	return (distx*distx + disty*disty);
	}

/*  [ 3 ] = [ 1 2 ] [ -1 ] = [ (1 * -1) + (2 * 2) ]    
[ 5 ] = [ 3 4 ] [  2 ] = [ (3 * -1) + (4 * 2) ]  */
void mul_matrix2x2_vector2f(float result[2],float vec[2],float mat[4])
{
	int count, count2;
	float temp[2];
	copyvector2f(temp, vec);
	zerovector2f(result);
	for (count = 0; count < 2; count++)
	for (count2 = 0; count2 < 2; count2++)
	result[count] += temp[count2] * mat[(count2*2)+count];
	//count*2+count for [ 0 2 ] matrices
	//count*2+ count2 for [ 0 1 ] matrices
	}
void mul_matrix3x3_vector3f(float result[3],float vec[3],float mat[9])
	{
	int count, count2;
	float temp[3];
	memcpy(temp, vec, 3*sizeof(float));
	memset(result, 0, 3*sizeof(float));
	for (count = 0; count < 3; count++)
	for (count2 = 0; count2 < 3; count2++)
		result[count] += temp[count2] * mat[(count2*3)+count];
	}
void mul_matrix4x4_vector4f(float result[4],float vec[4],float mat[16])
	{
	int count, count2;
	float temp[4];
	memcpy(temp, vec, 4*sizeof(float));
	memset(result, 0, 4*sizeof(float));
	for (count = 0; count < 4; count++)
	for (count2 = 0; count2 < 4; count2++)
		result[count] += temp[count2] * mat[(count2*4)+count];
	}
//
//SUB TRANSPOSE 3x3 in 4x4
//[ 0  1  2  3  ] = [ 0  4  8  3  ]   
//[ 4  5  6  7  ] = [ 1  5  9  7  ]   
//[ 8  9  10 11 ] = [ 2  6  10 11 ]   
//[ 12 13 14 15 ] = [ 12 13 14 15 ] */

INLINE void transpose_submatrix3x3_matrix4x4(float result[16], float mat[16])
	{
	float temp[16];
	memcpy(temp, mat, sizeof(float)*16);
	//transpose
	temp[1] = mat[4];
	temp[2] = mat[8];
	temp[4] = mat[1];
	temp[6] = mat[9];
	temp[8] = mat[2];
	temp[9] = mat[6];
	memcpy(result, temp, sizeof(float)*16);
	}

//[ 0 3 6 ] = [ 0 1 2 ] 
//[ 1 4 7 ] = [ 3 4 5 ] 
//[ 2 5 8 ] = [ 6 7 8 ] 
INLINE void transpose_matrix3x3(float result[9], float mat[9])
	{
	float temp[9];
	memcpy(temp, mat, sizeof(float)*9);
	temp[1]=mat[3];
	temp[2]=mat[6];
	temp[3]=mat[1];
	temp[5]=mat[7];
	temp[6]=mat[2];
	temp[7]=mat[5];
	memcpy(result,temp,sizeof(float)*9);
	}
//[ 0  4  8  12  ] = [ 0 4 8 ] 
//[ 1  5  9  13  ] = [ 1 5 9 ] 
//[ 2  6  10 14  ] = [ 2 6 10] 
//[ 3  7  11 15  ] */
INLINE void submatrix3x3_matrix4x4(float result[9], float mat[16])
	{
	result[0]=mat[0];
	result[1]=mat[1];
	result[2]=mat[2];
	result[3]=mat[4];
	result[4]=mat[5];
	result[5]=mat[6];
	result[6]=mat[8];
	result[7]=mat[9];
	result[8]=mat[10];
	}