int lineintersectline2d(float intersectpoint[2],float normal[2],float *scale,float startpoint[2],float endpoint[2],float vertex1[2],float vertex2[2]);
int sphereintersectline2d(float intersectpoint[2],float normal[2],float *scale,float point[2],float vertex1[2],float vertex2[2],float size);
int lineintersectcircle2d(float intersectpoint[2],float normal[2],float *scale,float vertex1[2],float vertex2[2],float point[2],float size);
int pointintersectcircle(float normal[2],float *scale,float point[2],float vertex[2],float radius);
