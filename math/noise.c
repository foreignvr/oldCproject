float lerp(float x,float y,float t)
  {
  return(x*(1.0f-t)+y*t);
  }

float cerp(float x,float y,float t)
  {
  t=(1.0f-cosf(t*pi))*0.5f;
  return(x*(1.0f-t)+y*t);
  }

float noise1d(int x)
  {
  int n;

  n=x;

  n=(n<<13)^n;

  n=n*n*15731+789221;
  n=(n*n+1376312589)&0x7FFFFFFF;

  return(1.0f-(float)n/1073741824.0f);
  }

float noise2d(int x,int y)
  {
  int n;

  n=x+y*57;

  n=(n<<13)^n;

  n=n*n*15731+789221;
  n=(n*n+1376312589)&0x7FFFFFFF;

  return(1.0f-(float)n/1073741824.0f);
  }

float lerpnoise2d(float x,float y,int mask)
  {
  int ix,iy;
  float frcx,frcy;
  float n[2][2];
  float result;

  ix=(int)x;
  iy=(int)y;

  frcx=x-(float)ix;
  frcy=y-(float)iy;

  n[0][0]=noise2d(ix,iy);
  n[0][1]=noise2d(((ix+1)&mask),iy);
  n[1][0]=noise2d(ix,((iy+1)&mask));
  n[1][1]=noise2d(((ix+1)&mask),((iy+1)&mask));

  result=lerp(lerp(n[0][0],n[0][1],frcx),lerp(n[1][0],n[1][1],frcx),frcy);

  return(result);
  }

float cerpnoise2d(float x,float y,int mask)
  {
  int ix,iy;
  float frcx,frcy;
  float n[2][2];
  float result;

  ix=(int)x;
  iy=(int)y;

  frcx=x-(float)ix;
  frcy=y-(float)iy;
  ix&=mask;
  iy&=mask;

  n[0][0]=noise2d(ix,iy);
  n[0][1]=noise2d(((ix+1)&mask),iy);
  n[1][0]=noise2d(ix,((iy+1)&mask));
  n[1][1]=noise2d(((ix+1)&mask),((iy+1)&mask));

  result=cerp(cerp(n[0][0],n[0][1],frcx),cerp(n[1][0],n[1][1],frcx),frcy);

  return(result);
  }

float getrandomfloat(void)
  {
  float value;

  value=noise1d(random_t.pos);
  random_t.pos++;

  return(value);
  }

void getrandomvector(float vec[3],float size,float minsize)
  {
  vec[0]=getrandomfloat();
  vec[1]=getrandomfloat();
  vec[2]=getrandomfloat();
  normalizevector3f(vec,vec);
  scalevector3f(vec,vec,(getrandomfloat()+1.0f)*size*0.5f+minsize);
  }
