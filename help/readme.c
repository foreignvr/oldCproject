void readmeloop(void)
  {
  int count;
  resetmenuitems();
  
  int currentUpdate = GetUpdateFrame();
  int nextUpdate = currentUpdate;
  char textfiles[4][32] = {"readme/version.txt","readme/maptutorial.txt","readme/gametutorial.txt","readme/credits.txt"};
  char textbuffers[4][128];

  // read text data from the disk
  for( count=0;count<4;count++ )
    {
    file = s3eFileOpen(textfiles[count], "rb");
    if (file != NULL)
      {
      s3eFileRead(textbuffers[count],128,1,file);
      textbuffers[count][MIN(s3eFileGetSize(file),127)]='\0';
      s3eFileClose(file);
      }
    else textbuffers[count][0]='\0';
    }

  while(!s3eDeviceCheckQuitRequest())
    {
    //(marmalade cluster)
      {
      while(!s3eDeviceCheckQuitRequest()) {
        if( GetUpdateFrame() != currentUpdate ) break;
        s3eDeviceYield(1);
        }
      Iw2DSurfaceClear(0xFF8C8C00);
      }
    //~(marmalde cluster)

    Iw2DSetColour(0xffffffff);
    Iw2DSetFont(font[FONT_MEDIUM]);
    
    numofmenuitems=0;
    
    createmenubutton("Back",208.0f,300.0f,64.0f,17.0f,FONT_MEDIUM,0.3f,0.3f,0.3f,1.0f);
    
    Iw2DDrawString("Version",CIwSVec2(40,3),CIwSVec2(161,25),IW_2D_FONT_ALIGN_CENTRE, IW_2D_FONT_ALIGN_TOP);
    createmenubutton(textbuffers[0],40.0f,24.0f,161.0f,100.0f,FONT_SMALL,0.3f,0.3f,0.3f,1.0f);
    setmenuitem(MO_DISABLE);
    
    Iw2DDrawString("Map Tutorial",CIwSVec2(280,3),CIwSVec2(161,25),IW_2D_FONT_ALIGN_CENTRE, IW_2D_FONT_ALIGN_TOP);
    createmenubutton(textbuffers[1],280.0f,24.0f,161.0f,100.0f,FONT_SMALL,0.3f,0.3f,0.3f,1.0f);
    setmenuitem(MO_DISABLE);
    
    Iw2DDrawString("Game Tutorial",CIwSVec2(40,161),CIwSVec2(161,25),IW_2D_FONT_ALIGN_CENTRE, IW_2D_FONT_ALIGN_TOP);
    createmenubutton(textbuffers[2],40.0f,183.0f,161.0f,100.0f,FONT_SMALL,0.3f,0.3f,0.3f,1.0f);
    setmenuitem(MO_DISABLE);
    
    Iw2DDrawString("Credits",CIwSVec2(280,161),CIwSVec2(161,25),IW_2D_FONT_ALIGN_CENTRE, IW_2D_FONT_ALIGN_TOP);
    createmenubutton(textbuffers[3],280.0f,183.0f,161.0f,100.0f,FONT_SMALL,0.3f,0.3f,0.3f,1.0f);
    setmenuitem(MO_DISABLE);
    
    checkmenuitems();
    drawmenuitems();
    
    Iw2DSurfaceShow();
    
    checkinput();
    
    if( menuitem[0].active )
      break;
    }
  resetmenuitems();
  }