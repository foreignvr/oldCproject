#include "s3e.h"
#include "Iw2D.h"
#include "IwResManager.h"

#define UPS 60
#define MAX_UPDATES 6

#define pi 3.14159265359f

s3eFile* file;

struct {
  int resolutionx;
  int resolutiony;
  } config;

#include "math.h"

/************************
			H
************************/
#include "game/debug.h"
#include "game/editor.h"
#include "game/frame.h"
#include "game/game.h"
#include "game/level.h"
#include "game/mainmenu.h"
#include "game/object.h"
#include "game/object_texture.h"
#include "game/player.h"
#include "game/tile.h"
#include "help/readme.h"
#include "marmalade/marmalade.h"
#include "math/intersect.h"
#include "math/intersect2d.h"
#include "math/noise.h"
#include "math/util.h"
#include "math/vector2.h"
#include "math/vector3.h"
#include "menu/display.h"
#include "menu/font.h"
#include "menu/menu.h"
/************************
			C
************************/
#include "game/debug.c"
#include "game/editor.c"
#include "game/frame.c"
#include "game/game.c"
#include "game/level.c"
#include "game/mainmenu.c"
#include "game/object.c"
#include "game/object_texture.c"
#include "game/player.c"
#include "game/tile.c"
#include "help/readme.c"
#include "marmalade/marmalade.c"
#include "math/intersect.c"
#include "math/intersect2d.c"
#include "math/noise.c"
#include "math/util.c"
#include "math/vector2.c"
#include "math/vector3.c"
#include "menu/display.c"
#include "menu/font.c"
#include "menu/menu.c"

int main()
{
  //pure initialization
  memset(&fuckedup,0,sizeof(int)*8);
  memset(&ffuckedup,0,sizeof(float)*8);
  config.resolutionx = 480;
  config.resolutiony = 320;
  //core
  //NOTE to SELF
  // 0xff00ff is internally tracked as alpha-replacer
  Iw2DInit();
  mainmenu();
  Iw2DTerminate();
  return 0;
}