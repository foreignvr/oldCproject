void checkmenuitems(void)
  {
  int count;
  for (count=0;count<numofmenuitems;count++)
    if ( (prevtouch.on && !touch.on) || menuitem[count].repeat)
      menuitem[count].active=0;
	for (count=0;count<numofmenuitems;count++)
    {
    menuitem[count].mouseover=0;
    menuitem[count].highlight=0;
    if (!menuitem[count].disabled)
      {
      //scrollbar diagram (jlh)
      // [1]     ||||||||||3||||||||||               [2]
      if( menuitem[count].type == MENUTYPE_SCROLLBAR )
        {
        if( menuitem[count].scrollbarmode == SCROLLBAR_VERTICAL )
        if( touch.x>=menuitem[count].x && touch.x<menuitem[count].x+menuitem[count].sizex )
          {
          if (touch.y>=menuitem[count].y)
          if (touch.y<menuitem[count].y+8)
            {
            menuitem[count].mouseover=1;
            if (touch.on && (!prevtouch.on || menuitem[count].repeat))
              menuitem[count].active=1;
            }
          if (touch.y>=menuitem[count].y+menuitem[count].sizey-8)
          if (touch.y<menuitem[count].y+menuitem[count].sizey)
            {
            menuitem[count].mouseover=2;
            if (touch.on && (!prevtouch.on || menuitem[count].repeat))
              menuitem[count].active=2;
            }
          if (touch.y>=menuitem[count].y+menuitem[count].value+8)
          if (touch.y<menuitem[count].y+menuitem[count].value+8+(menuitem[count].sizey-16-menuitem[count].scrollbarlen) )
            {
            if (touch.on && (!prevtouch.on || menuitem[count].repeat))
              menuitem[count].active=3;
            }
          }
        if( menuitem[count].scrollbarmode == SCROLLBAR_HORIZONTAL )
        if (touch.y>=menuitem[count].y && touch.y<menuitem[count].y+menuitem[count].sizey)
          {
          if (touch.x>=menuitem[count].x)
          if (touch.x<menuitem[count].x+8)
            {
            menuitem[count].mouseover=1;
            if (touch.on && (!prevtouch.on || menuitem[count].repeat))
              menuitem[count].active=1;
            }
          if (touch.x>=menuitem[count].x+menuitem[count].sizex-8)
          if (touch.x<menuitem[count].x+menuitem[count].sizex)
            {
            menuitem[count].mouseover=2;
            if (touch.on && (!prevtouch.on || menuitem[count].repeat))
              menuitem[count].active=2;
            }
          if (touch.x>=menuitem[count].x+menuitem[count].value+8)
          if (touch.x<menuitem[count].x+menuitem[count].value+8+(menuitem[count].sizex-16-menuitem[count].scrollbarlen) )
            {
            if (touch.on && (!prevtouch.on || menuitem[count].repeat))
              menuitem[count].active=3;
            }
          }
        }
      if( menuitem[count].type == MENUTYPE_BUTTON )
        {
        if (touch.x>=menuitem[count].x)
        if (touch.x<menuitem[count].x+menuitem[count].sizex)
        if (touch.y>=menuitem[count].y)
        if (touch.y<menuitem[count].y+menuitem[count].sizey)
          {
          menuitem[count].mouseover=1;
          if (touch.on && (!prevtouch.on || menuitem[count].repeat))
            menuitem[count].active=1;
          }
        }
      if (menuitem[count].active)
        menuitem[count].highlight=1;
      }

    if (menuitem[count].type==3 && ((*(int *)menuitem[count].inputpointer)&menuitem[count].value)==menuitem[count].value)
      menuitem[count].highlight=1;

    if (menuitem[count].type==4 && *(int *)menuitem[count].inputpointer==menuitem[count].value)
      menuitem[count].highlight=1;

    if (menuitem[count].sethighlight)
      menuitem[count].highlight=1;

    if (menuitem[count].highlight==1)
      menuitem[count].buttonheight=-1.0f;//+=(-1.0f-menuitem[count].buttonheight)*0.125f;
    else
      menuitem[count].buttonheight=1.0f;//+=(1.0f-menuitem[count].buttonheight)*0.125f;
    }

	for (currentmenuitem=0;currentmenuitem<numofmenuitems;currentmenuitem++)
    {
    if (menuitem[currentmenuitem].active && menuitem[currentmenuitem].function!=NULL)
      (*menuitem[currentmenuitem].function)();

    menuitem[currentmenuitem].prevactive=menuitem[currentmenuitem].active;
    }
  }

void createmenubutton(const char *label,float x,float y,float sizex,float sizey,int textsize,float red,float green,float blue,float alpha)
  {
  int count;
  float textlength;

  count=0;
  textlength=0.0f;

  strcpy(menuitem[numofmenuitems].label,label);
  menuitem[numofmenuitems].labelx=x+sizex*0.5f;
  menuitem[numofmenuitems].labely=y+sizey*0.5f;
  menuitem[numofmenuitems].labeltextsize=textsize;
	menuitem[numofmenuitems].x=x;
	menuitem[numofmenuitems].y=y;
	menuitem[numofmenuitems].red=red;
	menuitem[numofmenuitems].green=green;
	menuitem[numofmenuitems].blue=blue;
	menuitem[numofmenuitems].alpha=alpha;
  menuitem[numofmenuitems].type=MENUTYPE_BUTTON;
  menuitem[numofmenuitems].sizex=sizex;
  menuitem[numofmenuitems].sizey=sizey;
  menuitem[numofmenuitems].repeat=0;
  menuitem[numofmenuitems].disabled=0;
  menuitem[numofmenuitems].mouseover=0;
  menuitem[numofmenuitems].highlight=0;
  menuitem[numofmenuitems].sethighlight=0;
  menuitem[numofmenuitems].value=0;
  menuitem[numofmenuitems].inputpointer=NULL;
  menuitem[numofmenuitems].function=NULL;
  menuitem[numofmenuitems].cursornum=0;
  menuitem[numofmenuitems].background=0;
  numofmenuitems++;
  }

void setmenuitem(int option,...)
  {
  if (numofmenuitems==0)
    return;
  if (option==MO_DISABLE)
    {
    menuitem[numofmenuitems-1].disabled=1;
    if (!menuitem[numofmenuitems-1].sethighlight)
      {
      menuitem[numofmenuitems-1].red*=0.5f;
      menuitem[numofmenuitems-1].green*=0.5f;
      menuitem[numofmenuitems-1].blue*=0.5f;
      }
    }
  if (option==MO_SCROLLBAR)
    {
    menuitem[numofmenuitems-1].type=MENUTYPE_SCROLLBAR;
    menuitem[numofmenuitems-1].scrollbarlen = *((int *)(&option+1));
    menuitem[numofmenuitems-1].value = *((int *)(&option+2));
    fuckedup[3]=menuitem[numofmenuitems-1].scrollbarlen;
    if( menuitem[numofmenuitems-1].sizex > menuitem[numofmenuitems-1].sizey )
      {
      menuitem[numofmenuitems-1].scrollbarmode = SCROLLBAR_HORIZONTAL;
      menuitem[numofmenuitems-1].sizey = 10;
      }
    else
      {
      menuitem[numofmenuitems-1].scrollbarmode = SCROLLBAR_VERTICAL;
      menuitem[numofmenuitems-1].sizex = 10;
      }
    }
  }


void resetmenuitems(void)
  {
	int count;

	for (count=0;count<MAXMENUITEMS;count++)
    {
		menuitem[count].active=0;
    menuitem[count].prevactive=0;
    }
  }

void menutoggle(void)
  {
  *(int *)menuitem[currentmenuitem].inputpointer^=menuitem[currentmenuitem].value;
  menuitem[currentmenuitem].active=0;
  }

void menuset(void)
  {
  *(int *)menuitem[currentmenuitem].inputpointer=menuitem[currentmenuitem].value;
  menuitem[currentmenuitem].active=0;
  }


uint32 ABGRtoHex(float a, float b, float g, float r)
  {
  int A = ((uint32)floor(a*255));
  int B = ((uint32)floor(b*255));
  int G = ((uint32)floor(g*255));
  int R = ((uint32)floor(r*255));

  return (A<<24)+(B<<16)+(G<<8)+(R);
  }