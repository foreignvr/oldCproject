void drawmenuitems(void)
  {
  float color[4];
  int count,count2;
  float tint;
  for( count=0;count<numofmenuitems;count++)
    {
    if( menuitem[count].type == MENUTYPE_SCROLLBAR )
      {
      int button_gfx[2];
      int button2_pos[2];
      int scrollmiddle;
      //arrow graphic setup
      if( menuitem[count].scrollbarmode == SCROLLBAR_VERTICAL )
        {
        button2_pos[0] = (iwsfixed)(menuitem[count].x+menuitem[count].sizex-10);
        button2_pos[1] = (iwsfixed)(menuitem[count].y+menuitem[count].sizey-8);
        // up arrow
        if( menuitem[count].active == 1 ) button_gfx[0] = GUI_SCROLLUP_DEPRESS;
        else if( menuitem[count].mouseover == 1 ) button_gfx[0] = GUI_SCROLLUP_HOVER;
        else button_gfx[0] = GUI_SCROLLUP_NORMAL;
        // down arrow
        if( menuitem[count].active == 2 ) button_gfx[1] = GUI_SCROLLDOWN_DEPRESS;
        else if( menuitem[count].mouseover == 2 ) button_gfx[1] = GUI_SCROLLDOWN_HOVER;
        else button_gfx[1] = GUI_SCROLLDOWN_NORMAL;
        }
      if( menuitem[count].scrollbarmode == SCROLLBAR_HORIZONTAL )
        {
        button2_pos[0] = (iwsfixed)(menuitem[count].x+menuitem[count].sizex-8);
        button2_pos[1] = (iwsfixed)(menuitem[count].y+menuitem[count].sizey-10);
        // left arrow
        if( menuitem[count].active == 1 ) button_gfx[0] = GUI_SCROLLLEFT_DEPRESS;
        else if( menuitem[count].mouseover == 1 ) button_gfx[0] = GUI_SCROLLLEFT_HOVER;
        else button_gfx[0] = GUI_SCROLLLEFT_NORMAL;
        // right arrow
        if( menuitem[count].active == 2 ) button_gfx[1] = GUI_SCROLLRIGHT_DEPRESS;
        else if( menuitem[count].mouseover == 2 ) button_gfx[1] = GUI_SCROLLRIGHT_HOVER;
        else button_gfx[1] = GUI_SCROLLRIGHT_NORMAL;
        }
      if( menuitem[count].scrollbarmode == SCROLLBAR_VERTICAL )
        {
        // scroll bar background
        Iw2DDrawImageRegion(GUI_CHUNK_TEXTURE,CIwSVec2((iwsfixed)menuitem[count].x,(iwsfixed)(menuitem[count].y+8)),
          *GUI_TEXTURE_OFFSETS[GUI_SCROLLBAR_BGBUMPER_VERTICAL],*GUI_TEXTURE_DIMENSIONS[GUI_SCROLLBAR_BGBUMPER_VERTICAL]);
        for (count2 = (int)(menuitem[count].y+9); count2 < (int)(menuitem[count].y+menuitem[count].sizey-9);count2++)
          Iw2DDrawImageRegion(GUI_CHUNK_TEXTURE,CIwSVec2((iwsfixed)menuitem[count].x,count2),
            *GUI_TEXTURE_OFFSETS[GUI_SCROLLBAR_BACKGROUND_VERTICAL],*GUI_TEXTURE_DIMENSIONS[GUI_SCROLLBAR_BACKGROUND_VERTICAL]);
        Iw2DDrawImageRegion(GUI_CHUNK_TEXTURE,CIwSVec2((iwsfixed)menuitem[count].x,(iwsfixed)(menuitem[count].y+menuitem[count].sizey-9)),
          *GUI_TEXTURE_OFFSETS[GUI_SCROLLBAR_BGBUMPER_VERTICAL],*GUI_TEXTURE_DIMENSIONS[GUI_SCROLLBAR_BGBUMPER_VERTICAL]);
        // scroll bar
        Iw2DDrawImageRegion(GUI_CHUNK_TEXTURE,CIwSVec2((iwsfixed)menuitem[count].x,(iwsfixed)(menuitem[count].y+menuitem[count].value+8)),
          *GUI_TEXTURE_OFFSETS[GUI_SCROLLBAR_BARSTART_VERTICAL],*GUI_TEXTURE_DIMENSIONS[GUI_SCROLLBAR_BARSTART_VERTICAL]);
        for (count2 = (int)(menuitem[count].y+menuitem[count].value+9);
          count2 < (int)(menuitem[count].y+menuitem[count].value+7+menuitem[count].sizey-16-menuitem[count].scrollbarlen);count2++)
          {
          Iw2DDrawImageRegion(GUI_CHUNK_TEXTURE,CIwSVec2((iwsfixed)menuitem[count].x,count2),
            *GUI_TEXTURE_OFFSETS[GUI_SCROLLBAR_BARSECTION_VERTICAL],*GUI_TEXTURE_DIMENSIONS[GUI_SCROLLBAR_BARSECTION_VERTICAL]);
          }
        scrollmiddle = (int)( (menuitem[count].y+menuitem[count].value+9) + ((menuitem[count].sizey-16-menuitem[count].scrollbarlen)*0.5f) -6 );
        Iw2DDrawImageRegion(GUI_CHUNK_TEXTURE, CIwSVec2((iwsfixed)menuitem[count].x,(iwsfixed)scrollmiddle),
          *GUI_TEXTURE_OFFSETS[GUI_SCROLLBAR_BARMIDDLE_VERTICAL],*GUI_TEXTURE_DIMENSIONS[GUI_SCROLLBAR_BARMIDDLE_VERTICAL]);
        Iw2DDrawImageRegion(GUI_CHUNK_TEXTURE,
          CIwSVec2((iwsfixed)menuitem[count].x,(iwsfixed)(menuitem[count].y+menuitem[count].value+7+menuitem[count].sizey-16-menuitem[count].scrollbarlen)),
          *GUI_TEXTURE_OFFSETS[GUI_SCROLLBAR_BARSTOP_VERTICAL],*GUI_TEXTURE_DIMENSIONS[GUI_SCROLLBAR_BARSTOP_VERTICAL]);
        }
      if( menuitem[count].scrollbarmode == SCROLLBAR_HORIZONTAL )
        {
        // scroll bar background
        Iw2DDrawImageRegion(GUI_CHUNK_TEXTURE,CIwSVec2((iwsfixed)(menuitem[count].x+8),(iwsfixed)menuitem[count].y),
          *GUI_TEXTURE_OFFSETS[GUI_SCROLLBAR_BGBUMPER_HORIZONTAL],*GUI_TEXTURE_DIMENSIONS[GUI_SCROLLBAR_BGBUMPER_HORIZONTAL]);
        for (count2 = (int)(menuitem[count].x+9); count2 < (int)(menuitem[count].x+menuitem[count].sizex-9);count2++)
          Iw2DDrawImageRegion(GUI_CHUNK_TEXTURE,CIwSVec2(count2,(iwsfixed)menuitem[count].y),
            *GUI_TEXTURE_OFFSETS[GUI_SCROLLBAR_BACKGROUND_HORIZONTAL],*GUI_TEXTURE_DIMENSIONS[GUI_SCROLLBAR_BACKGROUND_HORIZONTAL]);
        Iw2DDrawImageRegion(GUI_CHUNK_TEXTURE,CIwSVec2((iwsfixed)(menuitem[count].x+menuitem[count].sizex-9),(iwsfixed)menuitem[count].y),
          *GUI_TEXTURE_OFFSETS[GUI_SCROLLBAR_BGBUMPER_HORIZONTAL],*GUI_TEXTURE_DIMENSIONS[GUI_SCROLLBAR_BGBUMPER_HORIZONTAL]);
        Iw2DDrawImageRegion(GUI_CHUNK_TEXTURE,CIwSVec2((iwsfixed)menuitem[count].x,(iwsfixed)(menuitem[count].y+menuitem[count].value+8)),
          *GUI_TEXTURE_OFFSETS[GUI_SCROLLBAR_BARSTART_HORIZONTAL],*GUI_TEXTURE_DIMENSIONS[GUI_SCROLLBAR_BARSTART_HORIZONTAL]);
        for (count2 = (int)(menuitem[count].x+menuitem[count].value+9);
          count2 < (int)(menuitem[count].x+menuitem[count].value+7+menuitem[count].sizex-16-menuitem[count].scrollbarlen);count2++)
          {
          Iw2DDrawImageRegion(GUI_CHUNK_TEXTURE,CIwSVec2(count2,(iwsfixed)menuitem[count].y),
            *GUI_TEXTURE_OFFSETS[GUI_SCROLLBAR_BARSECTION_HORIZONTAL],*GUI_TEXTURE_DIMENSIONS[GUI_SCROLLBAR_BARSECTION_HORIZONTAL]);
          }
        scrollmiddle = (int)( (menuitem[count].x+menuitem[count].value+9) + ((menuitem[count].sizex-16-menuitem[count].scrollbarlen)*0.5f) -6 );
        Iw2DDrawImageRegion(GUI_CHUNK_TEXTURE, CIwSVec2((iwsfixed)scrollmiddle,(iwsfixed)menuitem[count].y),
          *GUI_TEXTURE_OFFSETS[GUI_SCROLLBAR_BARMIDDLE_HORIZONTAL],*GUI_TEXTURE_DIMENSIONS[GUI_SCROLLBAR_BARMIDDLE_HORIZONTAL]);
        Iw2DDrawImageRegion(GUI_CHUNK_TEXTURE,
          CIwSVec2((iwsfixed)(menuitem[count].x+menuitem[count].value+7+menuitem[count].sizex-16-menuitem[count].scrollbarlen),(iwsfixed)menuitem[count].y),
          *GUI_TEXTURE_OFFSETS[GUI_SCROLLBAR_BARSTOP_HORIZONTAL],*GUI_TEXTURE_DIMENSIONS[GUI_SCROLLBAR_BARSTOP_HORIZONTAL]);
        }
      //button draw
      Iw2DDrawImageRegion(GUI_CHUNK_TEXTURE,CIwSVec2((iwsfixed)menuitem[count].x,(iwsfixed)menuitem[count].y),
        *GUI_TEXTURE_OFFSETS[button_gfx[0]],*GUI_TEXTURE_DIMENSIONS[button_gfx[0]]);
      Iw2DDrawImageRegion(GUI_CHUNK_TEXTURE,CIwSVec2(button2_pos[0],button2_pos[1]),
        *GUI_TEXTURE_OFFSETS[button_gfx[1]],*GUI_TEXTURE_DIMENSIONS[button_gfx[1]]);
      }
    if( menuitem[count].type == MENUTYPE_BUTTON )
      {
      tint = 1.0f;
      //background bar
      color[0] = menuitem[count].red;
      color[1] = menuitem[count].green;
      color[2] = menuitem[count].blue;
      color[3] = menuitem[count].alpha;
      if( menuitem[count].active )
        tint = 1.7f;
      else if( menuitem[count].mouseover )
        tint = 1.3f;
      scalevector3f(color,color,tint);
      uint32 colour = ABGRtoHex( MIN(color[3],1.f),MIN(color[2],1.f),MIN(color[1],1.f),MIN(color[0],1.f) );
      Iw2DSetColour(colour);
      Iw2DFillRect( CIwSVec2((iwsfixed)menuitem[count].x,(iwsfixed)menuitem[count].y), CIwSVec2((iwsfixed)menuitem[count].sizex,(iwsfixed)menuitem[count].sizey) );
      Iw2DSetColour(0xFF828282);
      //top bar
      Iw2DFillRect( CIwSVec2((iwsfixed)menuitem[count].x,(iwsfixed)menuitem[count].y-1),
        CIwSVec2( (iwsfixed)menuitem[count].sizex,1 ) );
      //bottom bar
      Iw2DFillRect( CIwSVec2((iwsfixed)menuitem[count].x,(iwsfixed)(menuitem[count].y+menuitem[count].sizey)),
        CIwSVec2((iwsfixed)menuitem[count].sizex,1) );
      //left bar
      Iw2DFillRect( CIwSVec2((iwsfixed)menuitem[count].x-1,(iwsfixed)menuitem[count].y),
        CIwSVec2(1,(iwsfixed)menuitem[count].sizey) );
      //top bar
      Iw2DFillRect( CIwSVec2((iwsfixed)(menuitem[count].x+menuitem[count].sizex),(iwsfixed)menuitem[count].y),
        CIwSVec2(1,(iwsfixed)menuitem[count].sizey) );
      Iw2DSetColour(0xffffffff);
      Iw2DSetFont(font[menuitem[count].labeltextsize]);
      Iw2DDrawString(menuitem[count].label, CIwSVec2((iwsfixed)menuitem[count].x,(iwsfixed)menuitem[count].y),
        CIwSVec2((iwsfixed)menuitem[count].sizex,(iwsfixed)(menuitem[count].sizey-font[menuitem[count].labeltextsize]->GetHeight()*0.5f)),
        IW_2D_FONT_ALIGN_CENTRE, IW_2D_FONT_ALIGN_CENTRE);
      }
    }
  }


void intializeguitextures(void)
  {
  if( _intializedguitextures ) return;
  _intializedguitextures=1;
  
  GUI_CHUNK_TEXTURE = Iw2DCreateImage("gui/gui_assets.png");
  
  GUI_TEXTURE_DIMENSIONS[GUI_SCROLLUP_NORMAL] = new CIwSVec2(10,8);
  GUI_TEXTURE_OFFSETS[GUI_SCROLLUP_NORMAL] = new CIwSVec2(14,0);
  
  GUI_TEXTURE_DIMENSIONS[GUI_SCROLLUP_HOVER] = new CIwSVec2(10,8);
  GUI_TEXTURE_OFFSETS[GUI_SCROLLUP_HOVER] = new CIwSVec2(14,18);
  
  GUI_TEXTURE_DIMENSIONS[GUI_SCROLLUP_DEPRESS] = new CIwSVec2(10,8);
  GUI_TEXTURE_OFFSETS[GUI_SCROLLUP_DEPRESS] = new CIwSVec2(0,21);
  
  GUI_TEXTURE_DIMENSIONS[GUI_SCROLLDOWN_NORMAL] = new CIwSVec2(10,8);
  GUI_TEXTURE_OFFSETS[GUI_SCROLLDOWN_NORMAL] = new CIwSVec2(14,9);
  
  GUI_TEXTURE_DIMENSIONS[GUI_SCROLLDOWN_HOVER] = new CIwSVec2(10,8);
  GUI_TEXTURE_OFFSETS[GUI_SCROLLDOWN_HOVER] = new CIwSVec2(14,27);
  
  GUI_TEXTURE_DIMENSIONS[GUI_SCROLLDOWN_DEPRESS] = new CIwSVec2(10,8);
  GUI_TEXTURE_OFFSETS[GUI_SCROLLDOWN_DEPRESS] = new CIwSVec2(0,30);
  
  GUI_TEXTURE_DIMENSIONS[GUI_SCROLLLEFT_NORMAL] = new CIwSVec2(8,10);
  GUI_TEXTURE_OFFSETS[GUI_SCROLLLEFT_NORMAL] = new CIwSVec2(25,0);
  
  GUI_TEXTURE_DIMENSIONS[GUI_SCROLLLEFT_HOVER] = new CIwSVec2(8,10);
  GUI_TEXTURE_OFFSETS[GUI_SCROLLLEFT_HOVER] = new CIwSVec2(25,11);
  
  GUI_TEXTURE_DIMENSIONS[GUI_SCROLLLEFT_DEPRESS] = new CIwSVec2(8,10);
  GUI_TEXTURE_OFFSETS[GUI_SCROLLLEFT_DEPRESS] = new CIwSVec2(25,22);
  
  GUI_TEXTURE_DIMENSIONS[GUI_SCROLLRIGHT_NORMAL] = new CIwSVec2(8,10);
  GUI_TEXTURE_OFFSETS[GUI_SCROLLRIGHT_NORMAL] = new CIwSVec2(34,0);
  
  GUI_TEXTURE_DIMENSIONS[GUI_SCROLLRIGHT_HOVER] = new CIwSVec2(8,10);
  GUI_TEXTURE_OFFSETS[GUI_SCROLLRIGHT_HOVER] = new CIwSVec2(34,11);
  
  GUI_TEXTURE_DIMENSIONS[GUI_SCROLLRIGHT_DEPRESS] = new CIwSVec2(8,10);
  GUI_TEXTURE_OFFSETS[GUI_SCROLLRIGHT_DEPRESS] = new CIwSVec2(34,22);
  
  GUI_TEXTURE_DIMENSIONS[GUI_SCROLLBAR_SQUARESTOP] = new CIwSVec2(10,10);
  GUI_TEXTURE_OFFSETS[GUI_SCROLLBAR_SQUARESTOP] = new CIwSVec2(43,19);

  GUI_TEXTURE_DIMENSIONS[GUI_SCROLLBAR_BACKGROUND_VERTICAL] = new CIwSVec2(10,1);
  GUI_TEXTURE_OFFSETS[GUI_SCROLLBAR_BACKGROUND_VERTICAL] = new CIwSVec2(43,17);
  
  GUI_TEXTURE_DIMENSIONS[GUI_SCROLLBAR_BGBUMPER_VERTICAL] = new CIwSVec2(10,1);
  GUI_TEXTURE_OFFSETS[GUI_SCROLLBAR_BGBUMPER_VERTICAL] = new CIwSVec2(43,30);
  
  GUI_TEXTURE_DIMENSIONS[GUI_SCROLLBAR_BARSTART_VERTICAL] = new CIwSVec2(10,1);
  GUI_TEXTURE_OFFSETS[GUI_SCROLLBAR_BARSTART_VERTICAL] = new CIwSVec2(43,0);
  
  GUI_TEXTURE_DIMENSIONS[GUI_SCROLLBAR_BARSECTION_VERTICAL] = new CIwSVec2(10,1);
  GUI_TEXTURE_OFFSETS[GUI_SCROLLBAR_BARSECTION_VERTICAL] = new CIwSVec2(43,2);
  
  GUI_TEXTURE_DIMENSIONS[GUI_SCROLLBAR_BARMIDDLE_VERTICAL] = new CIwSVec2(10,10);
  GUI_TEXTURE_OFFSETS[GUI_SCROLLBAR_BARMIDDLE_VERTICAL] = new CIwSVec2(43,4);
  
  GUI_TEXTURE_DIMENSIONS[GUI_SCROLLBAR_BARSTOP_VERTICAL] = new CIwSVec2(10,1);
  GUI_TEXTURE_OFFSETS[GUI_SCROLLBAR_BARSTOP_VERTICAL] = new CIwSVec2(43,15);
  
  GUI_TEXTURE_DIMENSIONS[GUI_SCROLLBAR_BACKGROUND_HORIZONTAL] = new CIwSVec2(1,10);
  GUI_TEXTURE_OFFSETS[GUI_SCROLLBAR_BACKGROUND_HORIZONTAL] = new CIwSVec2(43,32);

  GUI_TEXTURE_DIMENSIONS[GUI_SCROLLBAR_BGBUMPER_HORIZONTAL] = new CIwSVec2(1,10);
  GUI_TEXTURE_OFFSETS[GUI_SCROLLBAR_BGBUMPER_HORIZONTAL] = new CIwSVec2(45,32);
  
  GUI_TEXTURE_DIMENSIONS[GUI_SCROLLBAR_BARSTART_HORIZONTAL] = new CIwSVec2(1,10);
  GUI_TEXTURE_OFFSETS[GUI_SCROLLBAR_BARSTART_HORIZONTAL] = new CIwSVec2(47,32);
  
  GUI_TEXTURE_DIMENSIONS[GUI_SCROLLBAR_BARSECTION_HORIZONTAL] = new CIwSVec2(1,10);
  GUI_TEXTURE_OFFSETS[GUI_SCROLLBAR_BARSECTION_HORIZONTAL] = new CIwSVec2(49,32);
  
  GUI_TEXTURE_DIMENSIONS[GUI_SCROLLBAR_BARMIDDLE_HORIZONTAL] = new CIwSVec2(10,10);
  GUI_TEXTURE_OFFSETS[GUI_SCROLLBAR_BARMIDDLE_HORIZONTAL] = new CIwSVec2(25,33);

  GUI_TEXTURE_DIMENSIONS[GUI_SCROLLBAR_BARSTOP_HORIZONTAL] = new CIwSVec2(1,10);
  GUI_TEXTURE_OFFSETS[GUI_SCROLLBAR_BARSTOP_HORIZONTAL] = new CIwSVec2(51,32);
  
  
  }

void freeguitextures(void)
  {
  int count;
  delete GUI_CHUNK_TEXTURE;
  for( count=0;count<NUMGUITEXTURES;count++ )
    {
    delete GUI_TEXTURE_DIMENSIONS[count];
    delete GUI_TEXTURE_OFFSETS[count];
    }
  }