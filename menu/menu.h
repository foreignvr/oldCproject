#define MAXMENUITEMS   256

enum { MENUTYPE_BUTTON, MENUTYPE_SCROLLBAR };
#define MO_DISABLE       1
#define MO_SCROLLBAR       2

#define SCROLLBAR_VERTICAL 0
#define SCROLLBAR_HORIZONTAL 1

void checkmenuitems(void);
void createmenubutton(const char *label,float x,float y,float sizex,float sizey,int textsize,float red,float green,float blue,float alpha);
void setmenuitem(int option,...);
void resetmenuitems(void);
void menutoggle(void);
void menuset(void);

int numofmenuitems;
struct
  {
	int type;
  char label[128];
  float labelx;
  float labely;
  int labeltextsize;
	unsigned int labelflags;
	float x;
	float y;
	float red;
	float green;
	float blue;
	float alpha;
	float sizex;
	float sizey;
  int repeat;
  int disabled;
	int mouseover;
	int highlight;
	int sethighlight;
	float buttonheight;
	int active;
  int prevactive;
  int value;
  int cursornum;
  int background;
  void *inputpointer;
  void (*function)();
  int scrollbarlen;
  int scrollbarmode;
  } menuitem[MAXMENUITEMS];

int currentmenuitem;

uint32 ABGRtoHex(float r, float g, float b, float a);