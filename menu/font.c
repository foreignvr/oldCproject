void initializefonts ( void )
  {
  font[FONT_LARGE] = Iw2DCreateFont("font/font_20pt.gxfont");
  font[FONT_MEDIUM]= Iw2DCreateFont("font/font_14pt.gxfont");
  font[FONT_SMALL] = Iw2DCreateFont("font/font_9pt.gxfont");
  font[FONT_XSMALL]= Iw2DCreateFont("font/font_6pt.gxfont");
  }
void freefonts(void)
  {
  delete font[FONT_LARGE];
  delete font[FONT_MEDIUM];
  delete font[FONT_SMALL];
  delete font[FONT_XSMALL];
  }
/*
CIwGxSurface *pSurface = new CIwGxSurface();
pSurface->CreateSurface(NULL, MAX_SURFACE_SIZE, MAX_SURFACE_SIZE, 0))
pSurface->MakeCurrent();
//render everything you want
CIwGxSurface::MakeDisplayCurrent(); 
*/