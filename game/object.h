#define MAXNUMOBJECTS 2048
#define FLAG_INVERSE_BUILDING_GRAPHIC (1<<0)
struct {
  int active;
  int anchor;
  int type;
  int dimensions[2];
  int flags;
  struct {
    int width;
    int height;
    }render;
  } object[MAXNUMOBJECTS+1];

int numobjects;

int createnewobject( int anchor, int type, int flags );

void dropexpiredobjects();