void drawworld(void)
  {
  //int count;
  //if Iw2DSetSurface == true
  // drawmode == layered
  drawtilelayer();
  drawobjectlayer();
  }

void drawtilelayer( void )
  {
  int count, count2, cellindex;
  Iw2DSetTransformMatrix(*WORLDOFFSETMATRIX[0]);
  for( count = 0; count<31; count++ )
  for( count2 = 0; count2<81; count2+=2 )
    {
    cellindex = CELLINDEX(count+player.view[0],count2+player.view[1]);
    drawtiletextureat( CELLTEXTURE(cellindex), CIwSVec2( (count<<4),(count2<<2) ) );
    }
  Iw2DSetTransformMatrix(*WORLDOFFSETMATRIX[1]);
  for( count = 0; count<31; count++ )
  for( count2 = 1; count2<81; count2+=2 )
    {
    cellindex = CELLINDEX(count+player.view[0],count2+player.view[1]);
    drawtiletextureat( CELLTEXTURE(cellindex), CIwSVec2( (count<<4),(count2<<2) ) );
    }
  Iw2DSetTransformMatrix(CIwMat2D::g_Identity);
  }

void drawobjectlayer( void )
  {
  int count, cellx, celly;
  for( count=0;count<numobjects;count++ )
    {
    cellx = CELLX(object[count].anchor)-player.view[0];
    celly = CELLY(object[count].anchor)-player.view[1];
    if( (celly&1) == 0 ) Iw2DSetTransformMatrix(*WORLDOFFSETMATRIX[0]);
    else Iw2DSetTransformMatrix(*WORLDOFFSETMATRIX[1]);
    drawobjecttextureat(object[count].type,CIwSVec2( cellx*16, celly*4) );
    }
  Iw2DSetTransformMatrix(CIwMat2D::g_Identity);
  }

void initializedrawing(void)
  {
  if(_intializeddrawing)return;
  _intializeddrawing=1;
  WORLDOFFSETMATRIX[0] = new CIwMat2D(CIwMat2D::g_Identity);
  WORLDOFFSETMATRIX[0]->SetTrans(CIwVec2(0,-4));
  WORLDOFFSETMATRIX[1] = new CIwMat2D(CIwMat2D::g_Identity);
  WORLDOFFSETMATRIX[1]->SetTrans(CIwVec2(-8,-4));
  }

void freedrawing(void)
  {
  delete WORLDOFFSETMATRIX[0];
  delete WORLDOFFSETMATRIX[1];
  }