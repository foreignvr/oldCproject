void intializeobjecttextures(void)
  {
  if( _intializedobjecttextures ) return;
  _intializedobjecttextures=1;
  OBJECT_CHUNK_TEXTURE = Iw2DCreateImage("objects/object_assets.png");
  
  OBJECT_TEXTURE_DIMENSIONS[NATURE_TREES0_1X1] = new CIwSVec2(10,17);
  OBJECT_TEXTURE_OFFSETS[NATURE_TREES0_1X1] = new CIwSVec2(0,0);

  OBJECT_TEXTURE_DIMENSIONS[NATURE_ROCK0_2X2] = new CIwSVec2(31,25);
  OBJECT_TEXTURE_OFFSETS[NATURE_ROCK0_2X2] = new CIwSVec2(686,0);

  OBJECT_TEXTURE_DIMENSIONS[COMMERCE_SUBWAY_3X3] = new CIwSVec2(38,29);
  OBJECT_TEXTURE_OFFSETS[COMMERCE_SUBWAY_3X3] = new CIwSVec2(0,64);
  }

void freeobjecttextures(void)
  {
  int count;
  delete OBJECT_CHUNK_TEXTURE;
  for(count=0;count<NUMOBJECTTEXTURES;count++)
    {
    delete OBJECT_TEXTURE_DIMENSIONS[count];
    delete OBJECT_TEXTURE_OFFSETS[count];
    }
  }


void drawobjecttextureat( int object, CIwSVec2 pos )
  {
    //fixme
  int size[2];
  getobjectdimensions( size, object );
  int xposition = (pos.x - (OBJECT_TEXTURE_DIMENSIONS[object]->x>>1)) + (size[0]<<3);
  int yposition = (pos.y - OBJECT_TEXTURE_DIMENSIONS[object]->y)+ (size[1]<<2);
  //Iw2DDrawImageRegion(OBJECT_CHUNK_TEXTURE,CIwSVec2(xposition,yposition),*OBJECT_TEXTURE_OFFSETS[object],*OBJECT_TEXTURE_DIMENSIONS[object]);
  }

void getobjectdimensions( int result[2], int type )
  {
  if( type < OBJECT_DIVIDE_2X2 ) { result[0] = result[1] = 1; return; }
  if( type < OBJECT_DIVIDE_3X3 ) { result[0] = result[1] = 2; return; }
  if( type < OBJECT_DIVIDE_4X4 ) { result[0] = result[1] = 3; return; }
  result[0] = result[1] = -1;
  }