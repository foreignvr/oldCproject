void mainmenu(void)
  {
  int count;
  Iw2DSetUseMipMapping(false);

  initializefonts();
  
  resetmenuitems();
  
  int currentUpdate = GetUpdateFrame();
  int nextUpdate = currentUpdate;
  
  while(!s3eDeviceCheckQuitRequest())
    {
    //(marmalade cluster)
      {
      while(!s3eDeviceCheckQuitRequest()) {
        if( GetUpdateFrame() != currentUpdate ) break;
        s3eDeviceYield(1);
        }
      Iw2DSurfaceClear(0xFF8C8C00);
      }
    //~(marmalde cluster)
    
    numofmenuitems=0;
    
    count=267;
    createmenubutton("Exit",208.0f,(float)count,64.0f,17.0f,FONT_MEDIUM,0.3f,0.3f,0.3f,1.0f);
    count-=20;
    
    createmenubutton("Readme",208.0f,(float)count,64.0f,17.0f,FONT_MEDIUM,0.3f,0.3f,0.3f,1.0f);
    count-=20;
    
    createmenubutton("Level Editor",208.0f,(float)count,64.0f,17.0f,FONT_MEDIUM,0.3f,0.3f,0.3f,1.0f);
    count-=20;
    
    createmenubutton("Game",208.0f,(float)count,64.0f,17.0f,FONT_MEDIUM,0.3f,0.3f,0.3f,1.0f);
    count-=20;
    
    createmenubutton("www.poxpower.com",168.0f,300.0f,144.0f,10.0f,FONT_XSMALL,0.3f,0.3f,0.3f,1.0f);
    
    Iw2DSetColour(0xffffffff);
    Iw2DSetFont(font[FONT_XSMALL]);
    Iw2DDrawString("(C) just kidding there's no copyright 2012",CIwSVec2(168,285),
      CIwSVec2(Iw2DGetStringWidth("(C) just kidding there's no copyright 2012"),10),IW_2D_FONT_ALIGN_CENTRE, IW_2D_FONT_ALIGN_CENTRE);

    checkmenuitems();
    
    currentUpdate = nextUpdate;
    /***************
        RENDER
    ************/
    drawmenuitems();
    
    debug_drawmessedup();
    Iw2DSurfaceShow();
    
    checkinput();
    
    // Exit
    if( menuitem[0].active )
      {
      menuitem[0].active=0;
      s3eDeviceRequestQuit();
      }
    // Read Me
    if( menuitem[1].active )
      {
      menuitem[1].active=0;
      readmeloop();
      }
    // Level Editor
    if( menuitem[2].active )
      {
      menuitem[2].active=0;
      reseteditor();
      constructlevel(-1);
      editorloop();
      }
    // Game
    if( menuitem[3].active )
      {
      menuitem[3].active=0;
      resetgame();
      constructlevel(-1);
      gameloop();
      }
    //www . pox power . com
    if( menuitem[4].active )
      {
      menuitem[4].active=0;
      }
    }
  freefonts();
  freetiletextures();
  freedrawing();
  freeguitextures();
  freeobjecttextures();
  }