void checkinput(void)
  {
  memcpy(&prevtouch,&touch,sizeof(touch));

  s3ePointerUpdate();
  s3eKeyboardUpdate();
  s3eDeviceYield();

  touch.on = (s3ePointerGetState(S3E_POINTER_BUTTON_SELECT) & S3E_POINTER_STATE_DOWN );
  touch.x = s3ePointerGetX();
  touch.y = s3ePointerGetY();
  touch.xdif = touch.x-prevtouch.x;
  touch.ydif = touch.y-prevtouch.y;
  }
