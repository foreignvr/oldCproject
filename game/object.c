void dropexpiredobjects()
  {
  int count,count2,newnumobjects;
  for(count=0;count<numobjects;count++)
    {
    if( ! object[count].active )
      newnumobjects = count+1;
    else
      {
      for(count2=count;count2<numobjects;count2++)
      if( object[count2].active )
        {
        memcpy(&object[count],&object[count2],sizeof(object[0]));
        object[count2].active = 0;
        break;
        }
      }
    }
  numobjects = newnumobjects;
  }

int createnewobject( int anchor, int type, int flags)
  {
  object[numobjects].active=1;
  object[numobjects].anchor=anchor;
  object[numobjects].type=type;
  object[numobjects].flags=flags;
  object[numobjects].render.width = 100;
  object[numobjects].render.height = 100;
  changeobject(anchor,numobjects);
  numobjects++;
  return (numobjects-1);
  }