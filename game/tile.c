void intializetiletextures(void)
  {
  if( _intializedtiletextures ) return;
  _intializedtiletextures=1;
  TILE_CHUNK_TEXTURE = Iw2DCreateImage("tiles/tile_assets.png");
  
  TILE_TEXTURE_OFFSETS[TILE_ASPHALT] = new CIwSVec2(0,0);
  TILE_TEXTURE_DIMENSIONS[TILE_ASPHALT] = new CIwSVec2(16,7);
  TILE_TEXTURE_OFFSETS[TILE_GRASS] = new CIwSVec2(16,0);
  TILE_TEXTURE_DIMENSIONS[TILE_GRASS] = new CIwSVec2(16,7);
  TILE_TEXTURE_OFFSETS[TILE_SAND] = new CIwSVec2(32,0);
  TILE_TEXTURE_DIMENSIONS[TILE_SAND] = new CIwSVec2(16,7);
  TILE_TEXTURE_OFFSETS[TILE_DARKWATER] = new CIwSVec2(48,0);
  TILE_TEXTURE_DIMENSIONS[TILE_DARKWATER] = new CIwSVec2(16,7);
  TILE_TEXTURE_OFFSETS[TILE_WATER] = new CIwSVec2(0,8);
  TILE_TEXTURE_DIMENSIONS[TILE_WATER] = new CIwSVec2(16,7);
  TILE_TEXTURE_OFFSETS[TILE_EMPTY] = new CIwSVec2(16,8);
  TILE_TEXTURE_DIMENSIONS[TILE_EMPTY] = new CIwSVec2(16,7);
  TILE_TEXTURE_OFFSETS[TILE_SELECT0] = new CIwSVec2(32,8);
  TILE_TEXTURE_DIMENSIONS[TILE_SELECT0] = new CIwSVec2(16,7);
  TILE_TEXTURE_OFFSETS[TILE_SELECT1] = new CIwSVec2(48,8);
  TILE_TEXTURE_DIMENSIONS[TILE_SELECT1] = new CIwSVec2(16,7);
  }

void freetiletextures(void)
  {
  int count;
  delete TILE_CHUNK_TEXTURE;
  for ( count=0; count<8; count++ )
    {
    delete TILE_TEXTURE_OFFSETS[count];
    delete TILE_TEXTURE_DIMENSIONS[count];
    }
  }


INLINE void drawtiletextureat( int tile, CIwSVec2 pos )
  {
  Iw2DDrawImageRegion(TILE_CHUNK_TEXTURE,pos,*TILE_TEXTURE_OFFSETS[tile],*TILE_TEXTURE_DIMENSIONS[tile]);
  }