#define TILE_GRASS      0
#define TILE_ASPHALT    1
#define TILE_SAND       2
#define TILE_WATER      3
#define TILE_DARKWATER  4
#define TILE_EMPTY      5
#define TILE_SELECT0    6
#define TILE_SELECT1    7
#define NUMTILETEXTURES 8

void intializetiletextures(void);
void freetiletextures(void);


CIw2DImage* TILE_CHUNK_TEXTURE;
INLINE void drawtiletextureat( int, CIwSVec2 );

//drawing prefabs
CIwSVec2* TILE_TEXTURE_DIMENSIONS[NUMTILETEXTURES];
CIwSVec2* TILE_TEXTURE_OFFSETS[NUMTILETEXTURES];

int _intializedtiletextures = 0;