void constructlevel(int arg)
  {
  int count,count2,count3;
  memset( level.data, 0, sizeof(level.data) );
  if( arg == -1)
    {
    for( count = 0; count < 1024;count++ )
    for( count2 = 0; count2 < 1024;count2++ )
      {
      count3 = CELLINDEX(count,count2);
      if( count2 == 5 || count2 == 17 || (count == 15 && count2&1) )
        changetile( count3, TILE_ASPHALT );
      else
        changetile( count3, TILE_GRASS );
      }
    
    changetile( 38932, TILE_DARKWATER );
    for(count=0;count<10;count++)
      {
      changetile( CELL_SOUTH( 38932, count ), TILE_SELECT1 );
      changetile( CELL_NORTH( 38932, count ), TILE_SELECT1 );
      changetile( CELL_WEST( 38932, count ), TILE_SELECT0 );
      changetile( CELL_EAST( 38932, count ), TILE_SELECT0 );
      changetile( CELL_SOUTHWEST( 38932, count ), TILE_SELECT1 );
      changetile( CELL_SOUTHEAST( 38932, count ), TILE_SELECT1 );
      changetile( CELL_NORTHWEST( 38932, count ), TILE_SELECT0 );
      changetile( CELL_NORTHEAST( 38932, count ), TILE_SELECT0 );
      }
    }
  }

INLINE void changetile(int index, int value)
  {
  level.data[index] = value + (level.data[index] & LEVEL_INV_TILE_MASK);
  }

INLINE void changeobject(int index, int objectnum)
  {
  int size[2];
  int count, count2, ref;
  return;
  getobjectdimensions(size,object[objectnum].type);
  for( count=0;count<size[0];count++ )
  for( count2=0;count2<size[1];count2++ )
    {
    if(object[objectnum].flags&FLAG_INVERSE_BUILDING_GRAPHIC)
      ref = CELL_SOUTHWEST(CELL_SOUTHEAST(index,count),count2);
    else ref = CELL_SOUTHWEST(CELL_SOUTHEAST(index,count2),count);
    //clear_object(ref);
    level.data[ref] = (((count2*size[0])+count)<<LEVEL_REFRENCE_SHIFTS)
      + (object[objectnum].type<<LEVEL_OBJECT_SHIFTS) + (level.data[ref] & LEVEL_TILE_MASK);
    
    if( ref!=0 ) changetile(ref,TILE_EMPTY);
    }
  }
