//bitmasks
#define LEVEL_TILE_MASK       0x000000FF
#define LEVEL_INV_TILE_MASK   0xFFFFFF00
#define LEVEL_REFRENCE_MASK   0xFFE00000
#define LEVEL_FLIPOBJ_MASK    0x00100000
#define LEVEL_OBJECT_MASK     0x000FFF00
#define LEVEL_INV_OBJECT_MASK 0xFFF000FF
//bitshifts
#define LEVEL_OBJECT_SHIFTS    8
#define LEVEL_REFRENCE_SHIFTS  21
#define LEVEL_INVERSE_SHIFTS   20

#define NUMLEVELTILES 1048576

/**
 * Level.h should include the following:
 * memory declaration for the tiles,
 * NWSE direction functions
 * changetile + changeobject functions
 * @related bottom level logic
 *
 * jacob
 */
struct
  {
  int data[NUMLEVELTILES];
  } level;

void constructlevel(int);


INLINE void changetile(int, int);
void changeobject(int, int);

#define CELLTEXTURE(arg) ((level.data[(arg)]&LEVEL_TILE_MASK))
#define CELLINDEX(x,y) ( ((x)+((y)<<10)) )
#define CELLX(cell) ((cell)&1023)
#define CELLY(cell) ((cell)>>10)

//south, west, north, east etc should all use absolute distances from internal functions.
//for external/variable use - make a new function
#define CELL_SOUTH(index,distance) ( (index) + ( ((distance)<<10) << 1 ) )
#define CELL_NORTH(index,distance) ( (index) - ( ((distance)<<10) << 1 ) )
#define CELL_WEST(index,distance) ( (index) - (distance) )
#define CELL_EAST(index,distance) ( (index) + (distance) )
//these functions are marginally safer but not by enough to use outside determined functionality (jlh)
int CELL_SOUTHWEST(int index,int distance)
  {
  int half = distance>>1;
  if( (index >> 10) & 1) return index - (half+(distance&1)) + (distance<<10);
  else return index - (half) + (distance<<10);
  }
int CELL_SOUTHEAST(int index,int distance)
  {
  int half = distance>>1;
  if( (index >> 10) & 1) return index + (half) + (distance<<10);
  else return index + (half+(distance&1)) + (distance<<10);
  }
int CELL_NORTHWEST(int index,int distance)
  {
  int half = distance>>1;
  if( (index >> 10) & 1) return index - (half+(distance&1)) - (distance<<10);
  else return index - (half) - (distance<<10);
  }
int CELL_NORTHEAST(int index,int distance)
  {
  int half = distance>>1;
  if( (index >> 10) & 1) return index + (half) - (distance<<10);
  else return index + (half+(distance&1)) - (distance<<10);
  }